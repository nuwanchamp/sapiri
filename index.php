<?php include './header.php'; ?>
<main class="main-home">
    <section class="carousel-container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="ab-indicate-container"><!-- Carousel Indicating Container -->
                <a href="#carouselExampleIndicators" role="button" data-slide="prev" class="fa fa-angle-left carousel-ctrl"></a>
                <div class="carousel-indicators"><!-- Carousel Indicator list -->               
                    <div class="target active" data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></div>
                    <div class="target" data-target="#carouselExampleIndicators" data-slide-to="1"></div>
                    <div class="target" data-target="#carouselExampleIndicators" data-slide-to="2"></div>               
                </div><!--/ Carousel Indicatorlist -->
                <a  class="fa fa-angle-right carousel-ctrl" href="#carouselExampleIndicators" role="button" data-slide="next"></a>
            </div><!--/ Carousel Indicating Container -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="item-inner-wrap">
                        <img class="d-block w-100" src="images/slider-02.jpg" alt="First slide">
                        <div class="ab-caption">
                            <div class="d-block flex-protect">
                                <h1>New Books Arrived. Buy Now</h1>
                                <a href="#" class="sp-btn sp-btn-primary">See More</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="item-inner-wrap">
                        <img class="d-block w-100" src="images/slider-01.jpg" alt="Second slide">
                        <div class="ab-caption">
                            <div class="d-block flex-protect">
                                <h1>New Books Arrived. Buy Now</h1>
                                <a href="#" class="sp-btn sp-btn-primary">See More</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="item-inner-wrap">
                        <img class="d-block w-100" src="images/slider-03.jpg" alt="Third slide">
                        <div class="ab-caption">
                            <div class="d-block flex-protect">
                                <h1>New Books Arrived. Buy Now</h1>
                                <a href="#" class="sp-btn sp-btn-primary">See More</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="social-shopping-container">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-6 viber-whatapp">
                    <div class="text-wrap">
                        <p> Place your orders on <span class="text-uppercase">Viber & Whatapp</span> </p>
                        <span>071 547 2840</span>
                        <span><img  alt="Whatsapp" src="images/whatsapp-viber.png" /></span>
                    </div>
                </div>
                <div class="col-md-6 delivery">
                    <img alt="Delivery" src="images/icon-delivery.jpg"/>
                </div>
            </div>
        </div>
    </section>
    <section class="new-arrivals-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2><i class="fa fa-book"></i>Newly arrived books</h2>
                    <div class="show-case">
                        <a class="item-box">
                            <div class="inner-wrap">
                                <img class="product-img" src="images/book-01.jpg" alt="Book Name">
                                <div class="content-wrap">
                                    <h3 class="text-trancate">Dutch book brand name</h3>
                                    <span class="sp-price">Rs.450.00</span>
                                    <button class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                </div>
                            </div>
                        </a>
                        <a class="item-box">
                            <div class="inner-wrap">
                                <img class="product-img" src="images/book-03.jpg" alt="Book Name">
                                <div class="content-wrap">
                                    <h3 class="text-trancate">Dutch book brand name</h3>
                                    <span class="sp-price">Rs.450.00</span>
                                    <button class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                </div>
                            </div>
                        </a>
                        <a class="item-box">
                            <div class="inner-wrap">
                                <img class="product-img" src="images/book-02.jpg" alt="Book Name">
                                <div class="content-wrap">
                                    <h3 class="text-trancate">Dutch book brand name</h3>
                                    <span class="sp-price">Rs.450.00</span>
                                    <button class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                </div>
                            </div>
                        </a>
                        <a class="item-box">
                            <div class="inner-wrap">
                                <img class="product-img" src="images/book-03.jpg" alt="Book Name">
                                <div class="content-wrap">
                                    <h3 class="text-trancate">Dutch book brand name</h3>
                                    <span class="sp-price">Rs.450.00</span>
                                    <button class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                </div>
                            </div>
                        </a>
                        <a class="item-box">
                            <div class="inner-wrap">
                                <img class="product-img" src="images/book-02.jpg" alt="Book Name">
                                <div class="content-wrap">
                                    <h3 class="text-trancate">Dutch book brand name</h3>
                                    <span class="sp-price">Rs.450.00</span>
                                    <button class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="show-more">
                        <a href="#" class="sp-btn sp-btn-primary">See More books</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="gift-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-wrap">
                        <h2> Buy a Gift for <br/>your loved one</h2>
                        <img alt="Gift" src="images/gift-icon.png" />
                        <a href="#" class="sp-btn sp-btn-primary">See More</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gift-carousel-container ">
                        <div class="owl-carousel">
                            <div class="gift-item-box">
                                <div class="inner-wrap">
                                    <img src="images/gift-01.jpg" alt="Gift Image"/>
                                    <div class="item-content-wrap">
                                        <div class="sp-price">
                                            Rs 1200.00
                                        </div>
                                        <a href="#" class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="gift-item-box">
                                <div class="inner-wrap">
                                    <img src="images/gift-02.jpg" alt="Gift Image"/>
                                    <div class="item-content-wrap">
                                        <div class="sp-price">
                                            Rs 1200.00
                                        </div>
                                        <a href="#" class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="gift-item-box">
                                <div class="inner-wrap">
                                    <img src="images/gift-01.jpg" alt="Gift Image"/>
                                    <div class="item-content-wrap">
                                        <div class="sp-price">
                                            Rs 1200.00
                                        </div>
                                        <a href="#" class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="gift-item-box">
                                <div class="inner-wrap">
                                    <img src="images/gift-02.jpg" alt="Gift Image"/>
                                    <div class="item-content-wrap">
                                        <div class="sp-price">
                                            Rs 1200.00
                                        </div>
                                        <a href="#" class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="gift-item-box">
                                <div class="inner-wrap">
                                    <img src="images/gift-01.jpg" alt="Gift Image"/>
                                    <div class="item-content-wrap">
                                        <div class="sp-price">
                                            Rs 1200.00
                                        </div>
                                        <a href="#" class="sp-btn sp-btn-icon"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="owl-ctrl prev fa fa-angle-left"></a>
                        <a href="#" class="owl-ctrl next fa fa-angle-right"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
include './footer.php';

