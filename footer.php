<footer class='footer-section'>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="rights">
                    <p>
                        Copyrights &copy; Sapiri <?php echo date('Y');?> | All rights reserved.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="design">
                    <p>
                        Design by <a href="#" traget='_blank'>xy</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html> 