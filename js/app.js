(function ($) {
    $(document).ready(function () {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 10,
            items: 2,
        });
        $('.owl-ctrl.prev').on('click', function (e) {
            console.log('clicked');
            e.preventDefault();
            $('.owl-carousel').trigger('prev.owl.carousel', [300]);
        });
        $('.owl-ctrl.next').on('click', function (e) {
            console.log('clicked');
            e.preventDefault();
            $('.owl-carousel').trigger('next.owl.carousel', [300]);
        });
    });
})(jQuery);