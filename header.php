<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--Favicon-->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <!-- Styles -->

        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/fontawesome/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/owl-carousel/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>

        <!-- Scripts -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="/js/html5.js"></script>
        <![endif]-->
        <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="vendor/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
    </head>

    <body>
        <header class="header-section"> <!-- ribbon start -->
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#">
                    <img class="img-logo" alt="Logo" src="images/logo.jpg" /> 
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="active">
                            <a  href="#">Books <span class="sr-only">(current)</span></a>
                        </li>
                        <li>
                            <a  href="#">Stationary</a>
                        </li>
                        <li>
                            <a  href="#">Gifts</a>
                        </li>
                        <li> 
                            <a  href="#">Sports</a>
                        </li>
                        <li>
                            <a class="sp-btn" href="#">Place Your Order</a>
                        </li>
                       
                    </ul>
                    <ul class="shopping-options">
                        <li>
                            <a href="#" class="fa fa-search"></a>
                        </li>
                        <li>
                            <a href="#" class="fa fa-user"></a>
                        </li>
                        <li>
                            <a href="#" class="fa fa-shopping-cart"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header> <!-- ribbon end -->


